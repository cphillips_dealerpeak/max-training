<cfoutput><html>
<head>
	<title>Vehicle List</title>
</head>
<body>
	<h2>Vehicle List</h2>
	<table border="1" cellpadding="2" cellspacing="0">
		<thead>
			<tr>
				<th>Year</th>
				<th>Make</th>
				<th>Model</th>
				<th>Stock Number</th>
			</tr>
		</thead>
		<tbody>
			<!-- BEGIN: List of cars -->
			<cfset vehicles = application.beanFactory.getBean('vehicleDataCache').getVehicles() />
			<cfloop array="#vehicles#" index="vehicle">
			<tr>
				<td>#vehicle.getYear()#</td>
				<td>#vehicle.getMake()#</td>
				<td>#vehicle.getModel()#</td>
				<td><a href="?fuseaction=home.details&stock-number=#vehicle.getStockNumber()#">#vehicle.getStockNumber()#</a></td>
			</tr>
			</cfloop>
			<!-- END: List of cars -->
		</tbody>
	</table>
</body>
</html></cfoutput>