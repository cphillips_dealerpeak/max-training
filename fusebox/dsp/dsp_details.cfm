<cfset vehicle=application.beanFactory.getBean('vehicleDataCache').getVehicleFromCache( url['stock-number'] )  /><html>
<head>
	<title>Vehicle List</title>
	
	<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.js"></script>
	<script type="text/javascript">
		$(function(){
			$('.thumbnail').on('click', function(){
				$('#full-image').attr('src', $(this).attr('src'));
			});
		});
	</script>
</head>
<cfoutput><body>
	<h2>Vehicle Details</h2>
	<a href="?fuseaction=home.main"><< Back To List</a><br/><br/>
	<table border="1" cellpadding="2" cellspacing="0">
		<thead>
			<tr>
				<th>Details</th>
				<th>Photos</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td valign="top">
					<table border="0" cellpadding="2" cellspacing="0">
						<tr>
							<td><strong>Year:</strong></td>
							<td>#vehicle.getYear()#</td>
						</tr>
						<tr>
							<td><strong>Make:</strong></td>
							<td>#vehicle.getMake()#</td>
						</tr>
						<tr>
							<td><strong>Model:</strong></td>
							<td>#vehicle.getModel()#</td>
						</tr>
						<tr>
							<td><strong>StockNumber:</strong></td>
							<td>#vehicle.getStockNumber()#</td>
						</tr>
					</table>
				</td>
				<td width="400">
					<cfif arrayLen(vehicle.getImages()) GT 0>
						<div>
							<img id="full-image" width="400" src="photos/#vehicle.getImages()[1]#">
						</div>
						
						<cfloop array="#vehicle.getImages()#" index="image">
							<div style="float:left;padding:5px;"><img class="thumbnail" width="90" src="photos/#image#"></div>
						</cfloop>
					</cfif>
				</td>
			</tr>
		</tbody>
	</table>
</body>
</html>
</cfoutput>