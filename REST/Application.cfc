component accessors=true extends="max.com.BaseApplication" {
	public void function onRequestStart() {
		super.onRequestStart();
		if ( isDefined( "url.reinit" ) || isNull( application.REST ) ) {
			var relaxation = new relaxation.Relaxation( 'RestConfig.json' );
			relaxation.setBeanFactory( application.beanFactory );
			application.REST = relaxation;
		}
	}
	
	public void function onRequest( string targetPage ) {
		application.REST.handleRequest();
	}
}