component {
	
	this.name = "Max-Testing";
	
	this.ApplicationTimeout = CreateTimeSpan( 0,0,0,5 );
	this.SessionManagement = true;
	this.SessionTimeout = CreateTimeSpan( 0,0,10,0 );
	
	this.mappings = {
		'/coldspring': ExpandPath( '/max/com/coldspring' )
		,'/cfmongodb': ExpandPath( '/max/com/cfmongodb' )
		,'/com': ExpandPath( '/max/com' )
		,'/relaxation': ExpandPath( '/max/com/Relaxation' )
	};
	
	public void function onApplicationStart() {
		initializeBeanFactory();
	}
	
	public void function onRequestStart ( string targetPage ) {
		if ( isDefined( "url.reinit" ) ) {
			initializeBeanFactory();
		}
	}
	
	private void function initializeBeanFactory() {
		application.beanFactory = new coldspring.beans.xml.XmlBeanFactory( expandPath( "/max/coldspring.xml" ) );
		application.beanFactory.getBean( 'vehicleDataCache' ).buildCache();
		application.beanFactory.getBean( 'vehicleDataCache' ).copyCacheToMongoCollection();
		application.beanFactory.getBean( 'vehicleDataCache' ).copyCacheToSqlTable();
	}
}