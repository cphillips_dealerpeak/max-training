
component accessors=true output=false persistent=false {
	property string year;
	property string make;
	property string model;
	property string stockNumber;
	property array images;
	
	public void function populateData( string year, string make, string model, string stockNumber, array images ) {
		setYear( arguments.year );
		setMake( arguments.make );
		setModel( arguments.model );
		setStockNumber( arguments.stockNumber );
		setImages( arguments.images );
	}
	
	public void function addImages( any images ) {
		arrayAppend( getImages(), arguments.images, true );
	}
	
	public struct function getMemento() {
		return {
			'year': getYear(),
			'make': getMake(),
			'model': getModel(),
			'stockNumber': getStockNumber(),
			'images': getImages()
		};
	}
}