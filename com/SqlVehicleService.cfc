component output=false persistent=false {
	public SqlVehicleService function init() { 
		return this;
	}
	
	public array function getVehicles() {
		var vehicles = [];
		var vehicleQuery = new Query( sql="SELECT * FROM Vehicles", datasource="NewHireTest" ).execute().getResult();
		
		for ( var row in vehicleQuery ) {
			var vehicle = {
				'year': row.year,
				'make': row.make,
				'model': row.model,
				'stockNumber': row.stockNumber,
				'images': []
			};
			
			// now build the array of its images
			var vehicleId = row.vehicleId;			
			var imagesQuery = new Query( sql="SELECT * FROM VehicleImages WHERE vehicleId=:vehicleId", datasource="NewHireTest" );
			imagesQuery.addParam( name="vehicleId", cfsqltype="CF_SQL_INTEGER", value=vehicleId );
			var result = imagesQuery.execute().getResult();
			for( imageRow in result ) {
				arrayAppend( vehicle.images, imageRow.url );
			}
						
			arrayAppend( vehicles, vehicle );
		}
		
		return vehicles;
	}
	
	public struct function getVehicle( string stockNumber ) {
		var vehicleQuery = new Query( sql="SELECT TOP 1 * FROM Vehicles WHERE stockNumber=:stockNumber", datasource="NewHireTest" );
		vehicleQuery.addParam( name="stockNumber", value=arguments.stockNumber );
		vehicleQuery.setMaxRows( 1 );
		var vehicleQuery = vehicleQuery.execute();
		
		if( vehicleQuery.getPrefix().recordCount > 0 ) {
			var vehicleRow = vehicleQuery.getResult();
			var vehicle = {
				'id': vehicleRow.vehicleId,
				'year': vehicleRow.year,
				'make': vehicleRow.make,
				'model': vehicleRow.model,
				'stockNumber': vehicleRow.stockNumber
			};
			
			return vehicle;
		} else {
			// handle it not being found; e.g. return some kind of HTTP error code
			throw( type="NotFoundException", message="The requested vehicle was not found." );
		}
	}
}