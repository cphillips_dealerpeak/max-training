component accessors = true {
	property array vehicles;
	property string PHOTO_DIRECTORY_PATH;
	property component Mongo;
	
	public component function init() {
		setPHOTO_DIRECTORY_PATH( ExpandPath( '/max/photos' ));
		return this;
	}
	
	public void function copyCacheToMongoCollection( collection = "vehicles" ) {
		var vehicles = variables.Mongo.getDBCollection( collection );
		// clear out the collection
		vehicles.remove( {} );
		for ( var vehicleObject in getVehicles() ) {
			var vehicle = vehicleObject.getMemento();
			vehicles.save( vehicle );
		}
	}
	
	public void function copyCacheToSqlTable() {
		// clear out the tables
		new Query( sql="DELETE FROM NewHireTest.dbo.Vehicles", datasource="NewHireTest" ).execute();
		new Query( sql="DELETE FROM NewHireTest.dbo.VehicleImages", datasource="NewHireTest" ).execute();
		
		for ( var vehicle in getVehicles() ) {
			var vehicleQuery = new Query( sql="INSERT INTO NewHireTest.dbo.Vehicles ( year, make, model, stockNumber ) values ( :year, :make, :model, :stockNumber )", datasource="NewHireTest" );
			vehicleQuery.addParam( name="year", value=vehicle.getYear());
			vehicleQuery.addParam( name="make", value=vehicle.getMake());
			vehicleQuery.addParam( name="model", value=vehicle.getModel());
			vehicleQuery.addParam( name="stockNumber", value=vehicle.getStockNumber());
			var result = vehicleQuery.execute();
			
			var insertedId = result.getPrefix().generatedKey;
			for ( var image in vehicle.getImages() ) {
				var imageQuery = new Query( sql="INSERT INTO NewHireTest.dbo.VehicleImages ( vehicleId, url ) values ( :vehicleId, :url )", datasource="NewHireTest" );
				imageQuery.addParam( name="vehicleId", value=local.insertedId );
				imageQuery.addParam( name="url", value=local.image );
				imageQuery.execute();
			}
		}
	}
	
	public void function addVehicleToCache ( required com.Vehicle vehicleToAdd ) {
		try {
			var vehicle = getVehicleFromCache( arguments.vehicleToAdd.getStockNumber() );
			
			// if the vehicle was found, don't add it to the cache
			return;
		} catch ( NotFoundException e ) {
			arrayAppend( getVehicles(), arguments.vehicleToAdd );
		}
	}
	
	public void function buildCache () {
		setVehicles( [] );
		
		buildCacheFromDirectory();
	}
	
	public com.Vehicle function getVehicleFromCache ( required string stockNumber ) {
		for( var i = 1; i <= arrayLen( getVehicles()); ++i ) {
			if( getVehicles()[i].getStockNumber() == arguments.stockNumber ) {
				return getVehicles()[i];
			}
		}
		
		throw( type="NotFoundException", message="The requested vehicle was not found." );
	}
	
	private void function buildCacheFromDirectory() {
		var files = directoryList( getPHOTO_DIRECTORY_PATH(), false, "name" );
		
		for( var file in files ) {
			// use a regex on each so that non-matching ones aren't processed
			var patternMatchResult = REFind( "[0-9]+_[^_]+_[^_]+_[^_]+-[0-9]+\.jpg", file );
			
			if( patternMatchResult > 0 ) {
				var vehicleStruct = parseFileName( file );
				
				try {
					var vehicle = getVehicleFromCache( vehicleStruct.stockNumber );
					
					// if we reach this point, the vehicle was found.
					vehicle.addImages( vehicleStruct.images );
				} catch ( NotFoundException e ) {
					// if an exception was thrown, the vehicle wasn't in the cache. So let's add it.
					var vehicle = createObject( "com.Vehicle" );
					vehicle.populateData( argumentCollection = local.vehicleStruct );
					
					addVehicleToCache( vehicle );
				}
			}
		}
	}
	
	private struct function parseFileName( required string fileName ) {
		var vehicle = {};
		local.vehicle.images = [];
				
		var fileNameParseResult = reFindSubexpressions( "([0-9]+)_([^_]+)_([^_]+)_([^_]+)-([0-9]+)\.jpg", arguments.fileName, 1 );
		local.vehicle.year = fileNameParseResult[1];
		local.vehicle.make = fileNameParseResult[2];
		local.vehicle.model = fileNameParseResult[3];
		local.vehicle.stockNumber = fileNameParseResult[4];
		arrayAppend( local.vehicle.images, fileName );
		
		return local.vehicle;
	}
	
	// This is obviously a bad place for a generic function like this. Adding a utilities component seems overkill for this project, though.
	private array function reFindSubexpressions( required string regex, required string inputString, numeric startPosition = 1 ) {
		var subexpressions = [];
		var parseResult = REFind( arguments.regex, arguments.inputString, arguments.startPosition, true );
		
		// starting at 2 since we don't care about element 1 of REFind's returned array; we want the sub-expressions
		for( var i = 2; i <= arrayLen( local.parseResult.pos ); ++i ) {
			arrayAppend( subexpressions, mid( arguments.inputString, local.parseResult.pos[i], local.parseResult.len[i] ) );
		}
		
		return subexpressions;
	}
}
