component output=false persistent=false {
	property component mongoClient;
	
	public component function init ( required component mongoClient ) {
		setMongoClient( arguments.mongoClient );
		
		return this;
	}
	
	public void function setMongoClient( required component mongoClient ) {
		variables.mongoClient = arguments.mongoClient;
	}
	
	public array function getVehicles() {
		var vehiclesCollection = variables.mongoClient.getDBCollection( "vehicles" );
		var vehicles = vehiclesCollection.query().find().asArray();
		
		for( var vehicle in vehicles ) {
			vehicle._id = vehicle._id.toString();
		}
		
		return vehicles;
	}
	
	public any function getVehicle( required string stockNumber ) {
		var vehiclesCollection = variables.mongoClient.getDBCollection( "vehicles" );
		var vehicleResult = vehiclesCollection.query().$eq( "stockNumber", arguments.stockNumber ).find();
		
		if( ArrayLen( vehicleResult.asArray()) > 0 ) {
			var vehicle = vehicleResult.asArray()[1];
			vehicle._id = vehicle._id.toString();
			return vehicle;
		} else {
			return {};
		}
	}
}